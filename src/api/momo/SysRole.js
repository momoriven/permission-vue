import request from '@/utils/request'

// 是否禁用管理员按钮
export function disabledAdminRoleButton(data) {
  return request({
    url: '/platform/role/disabledAdminRoleButton/v1',
    method: 'post',
    data
  })
}
// 为角色授权权限
export function aclsToRole(data) {
  return request({
    url: '/platform/role/aclsToRole/v1',
    method: 'post',
    data
  })
}
// 为角色授权 权限 之前， 需要查看该角色拥有哪些权限点，以及当前登录用户可以操作哪些权限
export function roleHaveAclTree(data) {
  return request({
    url: '/platform/role/roleHaveAclTree/v1',
    method: 'post',
    data
  })
}
// 角色编辑
export function modify(data) {
  return request({
    url: '/platform/role/modify/v1',
    method: 'post',
    data
  })
}
// 角色编辑
export function showRole(data) {
  return request({
    url: '/platform/role/showRole/v1',
    method: 'post',
    data
  })
}
// 角色状态
export function status(data) {
  return request({
    url: '/platform/role/status/v1',
    method: 'post',
    data
  })
}
// 角色新增
export function save(data) {
  return request({
    url: '/platform/role/save/v1',
    method: 'post',
    data
  })
}
// 角色列表
export function roleList(data) {
  return request({
    url: '/platform/role/roleList/v1',
    method: 'post',
    data
  })
}

