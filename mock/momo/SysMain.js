const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin'
  },
  'editor-token': {
    roles: ['editor'],
    introduction: 'I am an editor',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
}

export default [
  // 获取验证码
  {
    url: '/platform/sysMain/images/captcha/v1',
    type: 'post',
    response: config => {
      return {
        'data': {
          'verUUidCode': '98e79f7b0fb84aed8bcdd3b172b9e688',
          'baseImg': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAwCAIAAABSYzXUAAAOj0lEQVR42u2be0xUVx7HTTabZrPZ%0D%0A7DZpNs1m02z2n83udrPdoG1lsaJGVmO1Puuj1vgqkWyNxUdtRSld01pdKVJ8MMDAyEMEGWFAEARE%0D%0AEVQUpIAD8iqCSAV5+ICOPMa7nzsHL8PMZWbUarrJnJzgufeee+45v+/5/X7f3++M4yR3+QmUcY4f%0D%0A19dL4eHS5s2Sr6/00UdSaKh08aJkNrvl9hxhOHlSBiAvT+ruli87O6XiYmnvXikoSLp50y265wID%0D%0AegAGiN6+oBA8un/fLb1nDwO2CD0Yq2RmSjqdW3rPHgb2u7BFqgUt2bjRLb1nDwM+2XFx2sFdfhxt%0D%0AUHUMijZAnNzlefgGeNFYJT1dOnjQLb1nBkPpzVJNqYYaaNCsDNOk1qSm16ZTS1pLeERtudvS9H0P%0D%0AjsHNWZ8hDADgofFwpfrE+SxIWuCb4ftp3qc7z+6MKY8BreKWYnDq7Ot0S/bH0QZq8BnN23t2rokN%0D%0A2poV5HfCb9Vx3wU6X6+wuZMipzkFaVL0pHePvbspZ1NYSRgqVdle2dOcLeUulOJeljTjpJjfSFkz%0D%0ApLo4yTzoxsB5MqOvT4qPH0lm7N8/Yovae9sbuhqALachJ8WYEnw+OKggCOWYmzh32mF1nLyjJr6f%0D%0Ashjt0ZzfnVn4iVE/6V7yq1JXlRuGca50GjRLRTekQ1ek5enS37XSL/4r/Xy39KtgaZ5equ1Sf6XH%0D%0A1IMGZNZlygjlblof9eqCxDmvR76uYtwiXvNLX4vS5DXmNfU0uThvlPWNyDcwjLuLdrv+loMy7+i8%0D%0AiVETJ8dMnpM4xz/bn73CyPnf5Xebup9gtKaenvDLlz/Ny/M7cWLa4cOeWu0bkZEeGs2EiIjYigqX%0D%0AYDA/lApbpNgqaW2WNPuY9Ouv5TorWVqRIYVellJrpaoO+f64XXKl4bxgiypD+HfIPITnSKg8tSJV%0D%0A66U9NF4Tzswe1f0emrWggixWpq5EBElXk2o7a/uH+m0Gu/vg7rqMdfTEIbXdb6PhpfV6Sgya7zQz%0D%0ATmRZZFFzUW5jruGaYde5XZtzNgMz9+cnzecSSHr7e50O1T80tOvcOSHxLadOZdXXA4n54UMeNXZ3%0D%0Ai8UGFRQ4gmHbGckrTnphj/RWvOSXLcVflZWgf0jlY+tPDcPwy70urBJ/0HuDf++YTGwQ5jE9NpbN%0D%0AYuzoGBgabGotMkT/Rczvn9ptNroiUEHi6Ao2ECARlnikvaJFNDS2nNrylDAkVCYwDmDYP7p1/5a+%0D%0AWs8nPLWeEyImfH7mcwfj9Pb3r0pLYyG+GRmtd+/aPDUNDoplohmOYECmSPZ8q/N5HzEOw0C9fsep%0D%0ABRln2XF3Zh85wiSCz59nQjYdxPzYQff670GO4V07Tu+AidnYMbbnNN2w42HbCrVAiIzR9UMXCAUW%0D%0ABH5R+EVaTRr72nV7wjj4M8d9BswDmEG2hcNxMliFf3a26lN0QiwTG+UIhndSZLFqykcZKEOdrBke%0D%0AMTJIrxx4ZGaaRmDQX3OuDW23atAApJzT0GD79F4TxGmsbaKgsj5r/cz4mdaQeEZ5ikb0leic+hxk%0D%0ARBuOgEMKuRAid9B6uoIB8mWbg5/jbgVNBeJz9nZSlOjycpbAVusbGLAHoLazs7C5WSxzil1adBQM%0D%0A/ymSxYofplxqky3PiyEj4qYuMwz3xF4pNzflO1mn6dTi+XERfD6zrk65WdLaqiktZeNMijykeIjJ%0D%0AMTGOhyI0EbIYHzH+zcg37R3+ZwWfYbvK25CIx9KUpa7AICwbUnbcDXMkPvHt99/aP8Xeemm1LIF1%0D%0AKTe7fvhBV17+QXq68M9KRWkcwSD2+G9DpZe/GSV9UX/2lVRjCcvu98tqodw/Wu1knbtOyPPbefas%0D%0AuLxw48acxETuLE1JCT+fb9S9BmfNbWzkzsKkJMdDRZdHC1kglPf07wn/uTpttb1H4e8neZ8Id+JU%0D%0AvmgSOkEb5HgXn49yQBMC8gNCL4Zm12cXNhfOiJshBld11ElXrzL/DzMzxeWQ2Yzzs5G+Uj8/c8YJ%0D%0AU5qZpAKAqKuHPyE3lJv/OuoEg4auLj7sExnSWx3LZbLRyOWshAT8c9X1qljd4h36sM05OTtOnwan%0D%0AouZmx6OtSlslZEG4jiURMIh9utqwet2JdR/nfuyt87ZxJ7BP5Ih9Ux2TDltzt8pB0kCf8hY0iTt1%0D%0AXXVoia5cp3x3UfIi1UEgRawru77eYuXMvhYngR5k1NbCi7DGwiXQRl0EcXIEg2lQRoKwAJJKwz9P%0D%0AZqh/i5IdQ6tlFRn1IxgQQDT2OIEhIB+V18RfzJUOv1RS8KXYDhkVxcvi9s+O2K3N3Aefc9GRQlqU%0D%0AEN380MxOR7jI6IP0D4AEPO6YZLaA0OnDXyCxDiTx9gT27HGcjaIixg62hQfuxNr6D/serSeum/Eh%0D%0A2bBkcXNvsTo1FOyDPUc7sqyM9orUVMhre28vYOAX2X8DYx/iOw/f8NhIHLdB6eiTTZYCw1cXnLyL%0D%0AxWQjYDSZkPlB95yYsGEfFbEvNnmDubPysWhlfEW8kIW9O0VMimSFRYIsCTsDWoQgNpB4x3jj86PK%0D%0Aog5cOsBle287PUVb1NiK2LPXz/JF4JyVMAvSLO4Dm+rcPC2OYcgiaGFyq9rb8Q0z4uKot/v6niqK%0D%0A7jZJL+2T/nBwOHpQojbqnyNkHuW4ELwwoUBLtIJ6KsaxoOlJ4l4EJ2SxXL8cbooqiO1vH4shUGgV%0D%0AJp5gW7kPTrxCuL7GsEZANeztNeODCoJQiG35I1FL+OVwlEB593bfbTwE4eRYcxNmx6aNmaVhuOaU%0D%0ASjqDAaqKxA0WgsNfa1dx+rpzwWHxmQfuV7GeCipPULASqjlE/2x/xN3Y3WjT/8PMD8ei+bgBSBfS%0D%0At6ZbWK3lx5cfKj2E9mCgQAJzB/d1JXgW2iDak6LhEZq7Dx4I7mTvCR4Phor2ESfcNyD9fv8IBkvS%0D%0AXBLcytRU5iGsP2RZwFDW1iaeHrx8eblez6QnRkXZ04nFx47Z6PLiY4sd5HRFSqOqvQraMz12OgpB%0D%0AALgxx8mJ+RTdlCUpS7bnb38n8R3rUHFu4ly8SOnN0m9KvlmYtFBV7UbP7RhzxghbnJOcKWB1Qi2e%0D%0AFgbPWNlX11tC0e1nRzDAe7e59usY6+0g5mQ9LSH9NQYDZlTE1TzqNplgtEQYPMKqjg6CzETOGGuF%0D%0AOyq+lL8bTm6gj75ab/0I++54hkhZpEPkDFKsDxC+nfD21MNTlRHwKNi394+/32NyRCW25uYy4UIL%0D%0A06vr6hKLFX9rOzufHAYRoAUWym3oEHgoMISVygaKkPuFPXIO6szYJNPaYgpIlEuKoNUYLtV3bTrb%0D%0AOa1uwq5l+mUIDjKDvFKMKRZzvFORICbF6foxOKgOfBRlElkj8W5cRRyGTvHMVMyX3wk/XPfNeyrn%0D%0AjolVVcwWjiQu02pqFLWGpLK9MAl4Sgzy7qKiFrt0kzoMBGi/C5M9s2nQ1jNT/3hIlj7ciYa4A2aq%0D%0ARex3sf2ZgZgWrGk4RomH+Wj4O3901CYUQuwmx0KcfWQ2u1gwVOgsd7gsai7CS+PDcQCuqCxRMU4C%0D%0AvUHbcDBolQhHlGMVXIWI2xVIgAeQrBPsSJkJr8/KUu7kNDT4xMWJVSAHnAdmeZleLwKIy6PPkNVh%0D%0A2HJ6xDPjioWsUQhcwhGjTJ9EWZslhw4vhkgLjquvcLnlqwJ8JioUAsoknlbfvk0cxxSVHAY9ucz/%0D%0A7jsRlNJ2dCRlibbglBBKTLxybAAMLvr8srYyuJONBzYNmhgWJbOmSdzBNKXWpEJ8rVkWgQh4wKCg%0D%0AYVhX9Ns+oWRdlLQSHrH5zh1HMNR0ynmLRanDl5kN8vEO0UOf3fjRFcNBHMGEakEB+aS+uvrRstvY%0D%0AEewR4aXRErYMHQ5cuiQ6nL1+XZnltMOHx0pVWueC4JFK0GsxgxNchAEUESj9qZivhMoEoU+8bpM8%0D%0AF/GKAkz/UD88CgWyDkRw6QH5X3nrtMpaVEudJadgf+qgAsPUI7JwK9odrcF4W9YM0FIsleqxhPgq%0D%0A4YyyRzA4IRcuzDt6VKgqoWZC5UgQp7MkKVGOARd+NQ6VRII4bUHzFf1wEQZ8CZ2rb1fTPyA/AJVi%0D%0ANICBFzGakkZtu8/WmTJWvpaAnEAEDB7hMXVCRMhqw9fFLcWqiVjWJRLJNvlwFRi8E2SxRn07hlq1%0D%0AqOSd/hGtDoNlK1WITw45kywIEfQLqueKHH0zfHHRe4v3sn5sizAyrsOABrDBrWM0NRtSiAMXIgYe%0D%0ABz2xS4/w8PLQbPfQbPPWzYUvgId1bpE1Kvk+ax6oAkPQOVmyM0fnOmu7pOASOblkA8CfNLK3cHZu%0D%0AXCoOpNp7ex2kPVZYggyq/bmV2hGKGSGKHyGwi0XeQiSFXIRBCQbFEYVyRsRQOO1kY7I1TaKGXgx1%0D%0AZVhjhxE8fGL9PDR7LHgs8YqeTgQTWZYZkH/KOufqZXX4owJDVcdwWrujT977/84ZYUTW9a+RYxIk%0D%0ANQtgxAoxCeJ7HIAIc5TzENieIBWqSWDVgjERp8eAgbwe5U6yuImhcFGZeNdwzYCMIKxE4+LgCHiW%0D%0ApixFydjgOHyBAf7ZaQRnrx+BBYem6LZ5aIIsJ+3h1AmR4TMTdBuzT2KK26z+a4I6U0LECBrOqprx%0D%0AfiteOtn42KkIY0eHCDWVim9YlJy8p7hYe+UKDe5sOHlSobPOslVZNimNNYY1K1JX0HbxhxppNWl0%0D%0AJuZAscaSI8EEfYgWn+bHHw1dDfsu7INbW882+Hywc8JqfdRsndb2y3biup2WktZWFAL6jB/GTC1N%0D%0ASYEUwWsD8vOLW1pcH4cwSjWlsad4j+uDHLh0AEEjlK25WwkOsCdAwq7H7W/M2ShIFHbvyX4jY18q%0D%0A2yu/PPelOMrFcLkURZffGj54wCItM8hHbIM/pf/yBrvHoAcWBK7LWIccRfQrTm8eq4i0K3vTP9t/%0D%0AcsxkQZZ84nxgwPBUQWF/3MIXMZsitf4Y5w3u8hyKGwY3DO7ihsENg7u4YXDD4C5uGNwwuIsbhv+L%0D%0A8j+HjdWEEPkv8wAAAABJRU5ErkJggg==',
          'imgType': 'data:image/png;base64,'
        },
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 用户退出
  {
    url: '/platform/sysUser/logout/v1',
    type: 'post',
    response: config => {
      return {
        'data': '安全退出成功',
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 密码登录
  {
    url: '/platform/sysMain/login/v1',
    type: 'post',
    response: config => {
      return {
        data: '登录成功',
        msg: '5cc9a0b69dfa4db6b0f87c9e45f5713b',
        oK: true,
        status: 200
      }
    }
  },

  // 获取菜单权限
  {
    url: '/platform/authority/dynamicMenu/v1',
    type: 'post',
    response: config => {
      return {
        'data': {
          'acls': [
            {
              'checked': true,
              'children': [
                {
                  'checked': true,
                  'children': [

                  ],
                  'createBy': '',
                  'createTime': '2019-08-02 15:11:28',
                  'defaultexpandedKeys': [

                  ],
                  'delFlag': 0,
                  'disabled': false,
                  'disabledFlag': 0,
                  'hasAcl': true,
                  'id': 195332810922397703,
                  'idStr': '195332810922397703',
                  'label': '首页',
                  'remark': '',
                  'sysAclAction': 'index',
                  'sysAclCode': '',
                  'sysAclComponentName': '',
                  'sysAclIcon': '',
                  'sysAclLevel': '0.195332810922397711',
                  'sysAclName': '首页',
                  'sysAclParentId': 195332810922397711,
                  'sysAclParentIdStr': '195332810922397711',
                  'sysAclPermissionCode': '1',
                  'sysAclRouter': '',
                  'sysAclSeq': 1,
                  'sysAclType': 1,
                  'sysAclUrl': '188696496605106236',
                  'updateBy': '李杰',
                  'updateTime': '2019-08-28 14:09:55',
                  'uuid': '188696496605106236'
                },
                {
                  'checked': true,
                  'children': [

                  ],
                  'createBy': '',
                  'createTime': '2019-08-02 15:11:02',
                  'defaultexpandedKeys': [

                  ],
                  'delFlag': 0,
                  'disabled': false,
                  'disabledFlag': 0,
                  'hasAcl': true,
                  'id': 195332810922397704,
                  'idStr': '195332810922397704',
                  'label': '产品提优',
                  'remark': '',
                  'sysAclAction': '',
                  'sysAclCode': '',
                  'sysAclComponentName': '',
                  'sysAclIcon': '',
                  'sysAclLevel': '0.195332810922397711',
                  'sysAclName': '产品提优',
                  'sysAclParentId': 195332810922397711,
                  'sysAclParentIdStr': '195332810922397711',
                  'sysAclPermissionCode': '1',
                  'sysAclRouter': '',
                  'sysAclSeq': 1,
                  'sysAclType': 1,
                  'sysAclUrl': '188696496605106237',
                  'updateBy': '李杰',
                  'updateTime': '2019-08-28 14:10:02',
                  'uuid': '38b8f82768e9407aa0abea4b83f3a0b0'
                },
                {
                  'checked': true,
                  'children': [
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '',
                          'createTime': '2019-04-15 11:09:36',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810909814787,
                          'idStr': '195332810909814787',
                          'label': '企业列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'SysEnterpriseList',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业列表',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/enterprise/sysUserGroupPage/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:42:40',
                          'uuid': '1b8ac96d955e4e78b8e0820b6e2fd890'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:22:09',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195872957032173568,
                          'idStr': '195872957032173568',
                          'label': '企业详情',
                          'remark': '',
                          'sysAclAction': 'detail',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业详情',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/detail/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:42:54',
                          'uuid': 'fecaff51af264f5aa32fce6dc2adfaba'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:23:35',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873316945399808,
                          'idStr': '195873316945399808',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/save/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:00',
                          'uuid': 'dc33944794704f0e94236edb2dff33e6'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:23:50',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873379507638272,
                          'idStr': '195873379507638272',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/modify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:06',
                          'uuid': 'd279c68cb9484eb8bcbb36e675b2f452'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:25:05',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873691517718528,
                          'idStr': '195873691517718528',
                          'label': '为企业授权(授权)',
                          'remark': '',
                          'sysAclAction': 'aclsToVip',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '为企业授权(授权)',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/aclsToEnterprise/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:12',
                          'uuid': '83dc22991ed14e8b8210118ca5bdbb4f'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:25:38',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873832010125312,
                          'idStr': '195873832010125312',
                          'label': '为企业授权(回显)',
                          'remark': '',
                          'sysAclAction': 'aclDetail',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '为企业授权(回显)',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/aclDetail/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:26',
                          'uuid': '821540d6a1d04b969ab7b6d6d5dce8f0'
                        },
                        {
                          'checked': true,
                          'children': [
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:13:07',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198060110281576448,
                              'idStr': '198060110281576448',
                              'label': '企业用户列表',
                              'remark': '',
                              'sysAclAction': 'userList',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户列表',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userList/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:30:16',
                              'uuid': 'a0dcd785acf74c19838299f1fad042fa'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:24:33',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198062985040760832,
                              'idStr': '198062985040760832',
                              'label': '企业用户新增',
                              'remark': '',
                              'sysAclAction': 'add',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户新增',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userAdd/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:31:08',
                              'uuid': 'c87fd3a3c2e941c2b7a50db3a9cd0e4c'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:25:08',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198063131954647040,
                              'idStr': '198063131954647040',
                              'label': '企业用户编辑',
                              'remark': '',
                              'sysAclAction': 'edit',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户编辑',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userModify/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:25:08',
                              'uuid': '4fffd3a677fd42fab14fd28e6c1f6b6c'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:27:20',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198063685430808576,
                              'idStr': '198063685430808576',
                              'label': '企业用户密码修改',
                              'remark': '',
                              'sysAclAction': 'userPwd',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户密码修改',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/sysUserPwd/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:31:47',
                              'uuid': '297c8af9b87d4ac6912630774fdbc3d6'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:28:39',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198064017837789184,
                              'idStr': '198064017837789184',
                              'label': '企业用户授权角色(回显)',
                              'remark': '',
                              'sysAclAction': 'userCheckRoles',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户授权角色(回显)',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userCheckRoles/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:28:39',
                              'uuid': '5562663686ee4c998c061ee692e4dc5e'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:28:56',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198064089065459712,
                              'idStr': '198064089065459712',
                              'label': '企业用户授权角色(授权)',
                              'remark': '',
                              'sysAclAction': 'rolesToUser',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户授权角色(授权)',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/rolesToUser/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:31:36',
                              'uuid': 'e7a7882e3881490e9c3eaba155273284'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:29:39',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198064269110153216,
                              'idStr': '198064269110153216',
                              'label': '企业用户状态',
                              'remark': '',
                              'sysAclAction': 'userStatus',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户状态',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userStatus/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:29:39',
                              'uuid': 'de0b28094cf5492f99f4991cb7071c03'
                            }
                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-28 11:08:36',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 198058969976147968,
                          'idStr': '198058969976147968',
                          'label': '企业用户管理',
                          'remark': '',
                          'sysAclAction': 'SysEnterpriseUserList',
                          'sysAclCode': '',
                          'sysAclComponentName': 'SysEnterpriseUserList',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业用户管理',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseUserList',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/enterprise/userManager/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:22:52',
                          'uuid': '52130fdd357642488e0040277e9cfbe7'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-28 11:48:52',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 198069104119255040,
                          'idStr': '198069104119255040',
                          'label': '企业状态',
                          'remark': '',
                          'sysAclAction': 'status',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业状态',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/status/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:48:52',
                          'uuid': 'f12a0448208a4e84ba4748050b9b4f25'
                        },
                        {
                          'checked': true,
                          'children': [
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-22 10:26:13',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 195873978903040000,
                              'idStr': '195873978903040000',
                              'label': '企业角色列表',
                              'remark': '',
                              'sysAclAction': 'listRole',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色列表',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleList/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:05:37',
                              'uuid': 'c4dd9b8b74154bfe85dedb7fa1d0da72'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:06:33',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198088655322091520,
                              'idStr': '198088655322091520',
                              'label': '企业角色新增',
                              'remark': '',
                              'sysAclAction': 'add',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色新增',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleAdd/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:06:33',
                              'uuid': 'f396973958a94b43971df535489d8e81'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:07:00',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198088766655696896,
                              'idStr': '198088766655696896',
                              'label': '企业角色编辑',
                              'remark': '',
                              'sysAclAction': 'edit',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色编辑',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleModify/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:07:00',
                              'uuid': 'cff2fa8bb01a4897b83e160f0d346f6c'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:07:51',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198088981710245888,
                              'idStr': '198088981710245888',
                              'label': '企业角色授权(查看)',
                              'remark': '',
                              'sysAclAction': 'roleHaveAclTree',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色授权(查看)',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleHaveAclTree/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:07:51',
                              'uuid': '69673415b9b549c98bbdd790d75b9da8'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:15:29',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198090905197088768,
                              'idStr': '198090905197088768',
                              'label': '企业角色授权(授权)',
                              'remark': '',
                              'sysAclAction': 'aclsToRole',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色授权(授权)',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/aclsToRole/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:15:29',
                              'uuid': '7f411f0a08ad46ca990f43cc6b852fcf'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:20:27',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198092153824612352,
                              'idStr': '198092153824612352',
                              'label': '企业角色状态',
                              'remark': '',
                              'sysAclAction': 'roleStatus',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色状态',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleStatus/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:20:27',
                              'uuid': 'd55579ea91964c9aa8c79e2caf5a16bb'
                            }
                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-28 13:05:04',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 198088282079367168,
                          'idStr': '198088282079367168',
                          'label': '企业角色管理',
                          'remark': '',
                          'sysAclAction': 'SysEnterpriseRoleList',
                          'sysAclCode': '',
                          'sysAclComponentName': 'SysEnterpriseRoleList',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业角色管理',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseRoleList',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/enterprise/roleManager/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 13:05:04',
                          'uuid': '303d4540ccd74c5c9daf6e0cac587564'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:10:29',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397705,
                      'idStr': '195332810922397705',
                      'label': '企业管理',
                      'remark': '',
                      'sysAclAction': 'manager',
                      'sysAclCode': '',
                      'sysAclComponentName': 'SysEnterpriseUserList',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '企业管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'SysEnterpriseUserList',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '188696496605106238',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:35',
                      'uuid': '188696496605106238'
                    },
                    {
                      'checked': true,
                      'children': [

                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:09:53',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397706,
                      'idStr': '195332810922397706',
                      'label': '部门管理',
                      'remark': '',
                      'sysAclAction': 'manager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '部门管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': '',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '188696496605106239',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-21 23:01:21',
                      'uuid': 'afae14c9a9d843c6b865872abd75955f'
                    },
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '',
                          'createTime': '2019-04-13 11:30:28',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810905620488,
                          'idStr': '195332810905620488',
                          'label': '角色列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'AuthorRole',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '角色列表',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/role/roleList/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:00',
                          'uuid': '2c0208809d974940b284ce4522d3e4dd'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:13:46',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195870845300772864,
                          'idStr': '195870845300772864',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/save/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:15',
                          'uuid': 'b30d24305f1f4def86bb8d3657b7fa4d'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:15:31',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195871284125634560,
                          'idStr': '195871284125634560',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/modify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:19',
                          'uuid': 'c893cb23b0b34c44b7f88888d289b936'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:15:52',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195871372877107200,
                          'idStr': '195871372877107200',
                          'label': '变更角色状态',
                          'remark': '',
                          'sysAclAction': 'status',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '变更角色状态',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/status/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:23',
                          'uuid': '7ba96f13f8bc4848b66f6ceca94c345e'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-27 18:39:46',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 197810122012102656,
                          'idStr': '197810122012102656',
                          'label': '权限给角色(回显)',
                          'remark': '',
                          'sysAclAction': 'aclShow',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '权限给角色(回显)',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/roleHaveAclTree/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:27',
                          'uuid': '8d5b6d89e1204305b70b3a455270e17f'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-27 18:41:49',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 197810639492747264,
                          'idStr': '197810639492747264',
                          'label': '权限给角色(授权)',
                          'remark': '',
                          'sysAclAction': 'aclAuthor',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '权限给角色(授权)',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/aclsToRole/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:31',
                          'uuid': '2ff2433314254f2b8b0c03ea356c7948'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:09:04',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397707,
                      'idStr': '195332810922397707',
                      'label': '角色管理',
                      'remark': '',
                      'sysAclAction': 'RoleManager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '角色管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'AuthorRole',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '/api/platform/role/catalogue/v1',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:07',
                      'uuid': 'f2d1ea1a116343b6ba3910d3734cbe72'
                    },
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '',
                          'createTime': '2019-04-13 11:26:58',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810905620484,
                          'idStr': '195332810905620484',
                          'label': '菜单列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'AclTree',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '菜单列表',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/acl/aclTree/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:29:39',
                          'uuid': '5f2056a6a51e441bb904dc6709a817e4'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:08:15',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869456721907712,
                          'idStr': '195869456721907712',
                          'label': '新增权限系统',
                          'remark': '',
                          'sysAclAction': 'moduleAdd',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '新增权限系统',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/saveSys/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:08:15',
                          'uuid': '3d6d98cd8f12482896a6371247fb212e'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:09:07',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869677409406976,
                          'idStr': '195869677409406976',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/modify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:09:07',
                          'uuid': '77aaeeccdcc447d1848017c45d960735'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:09:31',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869776344649728,
                          'idStr': '195869776344649728',
                          'label': '查询详情',
                          'remark': '',
                          'sysAclAction': 'detail',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '查询详情',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/detail/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:09:31',
                          'uuid': 'ef2b4f4b97cd4ad58ee83899ab36d19d'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:10:03',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869910314913792,
                          'idStr': '195869910314913792',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/save/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:10:03',
                          'uuid': '8670234a1f984917ae448df41d0101e1'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:08:01',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397708,
                      'idStr': '195332810922397708',
                      'label': '菜单管理',
                      'remark': '',
                      'sysAclAction': 'AclTreeManager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '菜单管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'AclTree',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '/api/platform/acl/catalogue/v1',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:13',
                      'uuid': 'b593168f64724ef2aadb55e90bf2fe53'
                    },
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-04-13 11:22:15',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810905620480,
                          'idStr': '195332810905620480',
                          'label': '用户列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'AuthorUser',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '用户列表',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/sysUser/sysUserList/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:29:16',
                          'uuid': 'c70a20be0a2e4a1889af5d69263dc972'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:55:55',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195866352387493888,
                          'idStr': '195866352387493888',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserAdd/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:08',
                          'uuid': '5d166d20fb6945a786dc68e9db7bd706'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:56:45',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195866564606693376,
                          'idStr': '195866564606693376',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserModify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:16',
                          'uuid': '149ffb4d4a734a4a891cfbab5ff0155e'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:57:31',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195866757108469760,
                          'idStr': '195866757108469760',
                          'label': '修改密码',
                          'remark': '',
                          'sysAclAction': 'editPwd',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '修改密码',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserPwd/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:22',
                          'uuid': '6e718ba386dd4da2bc8650b115203b0b'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:59:17',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195867199867588608,
                          'idStr': '195867199867588608',
                          'label': '角色给用户(回显)',
                          'remark': '',
                          'sysAclAction': 'roleShow',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '角色给用户(回显)',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/userCheckRoles/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:27',
                          'uuid': '651de34888fe4f8e870c4b66ad5f3259'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:59:45',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195867319661105152,
                          'idStr': '195867319661105152',
                          'label': '角色给用户(授权)',
                          'remark': '',
                          'sysAclAction': 'roleAuthor',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '角色给用户(授权)',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/rolesToUser/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:32',
                          'uuid': 'a0218f58b2854b84bd54608bef95453d'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:00:23',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195867479266955264,
                          'idStr': '195867479266955264',
                          'label': '状态',
                          'remark': '',
                          'sysAclAction': 'status',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '状态',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserStatus/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:39',
                          'uuid': 'cea452c89a77455ea4ed743a5b37a014'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:07:44',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397709,
                      'idStr': '195332810922397709',
                      'label': '用户管理',
                      'remark': '',
                      'sysAclAction': 'UserManager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '用户管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'AuthorUser',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '/api/platform/sysUser/catalogue/v1',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:20',
                      'uuid': 'e88c27d65f4a4e75bd5842175bfd6371'
                    }
                  ],
                  'createBy': '',
                  'createTime': '2019-08-02 15:07:11',
                  'defaultexpandedKeys': [

                  ],
                  'delFlag': 0,
                  'disabled': false,
                  'disabledFlag': 0,
                  'hasAcl': true,
                  'id': 195332810922397710,
                  'idStr': '195332810922397710',
                  'label': '权限管理',
                  'remark': '',
                  'sysAclAction': 'manager',
                  'sysAclCode': '',
                  'sysAclComponentName': 'Auth',
                  'sysAclIcon': '',
                  'sysAclLevel': '0.195332810922397711',
                  'sysAclName': '权限管理',
                  'sysAclParentId': 195332810922397711,
                  'sysAclParentIdStr': '195332810922397711',
                  'sysAclPermissionCode': '1',
                  'sysAclRouter': 'Auth',
                  'sysAclSeq': 1,
                  'sysAclType': 0,
                  'sysAclUrl': '61fcd23e1aad4a2581160de1fce545a7',
                  'updateBy': '李杰',
                  'updateTime': '2019-08-21 23:01:31',
                  'uuid': '61fcd23e1aad4a2581160de1fce545a7'
                }
              ],
              'createBy': '',
              'createTime': '2019-08-02 15:04:14',
              'defaultexpandedKeys': [

              ],
              'delFlag': 0,
              'disabled': false,
              'disabledFlag': 0,
              'hasAcl': true,
              'id': 195332810922397711,
              'idStr': '195332810922397711',
              'label': '后台管理模块',
              'remark': '',
              'sysAclAction': 'manager',
              'sysAclCode': '',
              'sysAclComponentName': '',
              'sysAclIcon': '',
              'sysAclLevel': '0',
              'sysAclName': '后台管理模块',
              'sysAclParentId': 0,
              'sysAclParentIdStr': '0',
              'sysAclPermissionCode': '1',
              'sysAclRouter': '',
              'sysAclSeq': 1,
              'sysAclType': -1,
              'sysAclUrl': '188696496605106244',
              'updateBy': '',
              'updateTime': '2019-08-02 15:04:16',
              'uuid': '61fcd23e1aad4a2581160de1fce545a6'
            }
          ]
        },
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },

  // get user info
  {
    url: '/user/info\.*',
    type: 'get',
    response: config => {
      const { token } = config.query
      const info = users[token]

      // mock error
      if (!info) {
        return {
          code: 50008,
          message: 'Login failed, unable to get user details.'
        }
      }

      return {
        code: 20000,
        data: info
      }
    }
  },

  // user logout
  {
    url: '/platform/sysMain/logout/v1',
    type: 'post',
    response: _ => {
      return {
        data: '安全退出成功',
        msg: '5cc9a0b69dfa4db6b0f87c9e45f5713b',
        oK: true,
        status: 200
      }
    }
  }
]
